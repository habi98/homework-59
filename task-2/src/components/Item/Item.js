import React, {Component} from 'react';
import './Item.css';

class Item extends Component {
    render() {
        return (
            <div className="item">
                <h3>{this.props.value}</h3>
            </div>
        );
    }
}

export default Item;