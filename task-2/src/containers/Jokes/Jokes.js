import React, {Component} from 'react';
import Item from "../../components/Item/Item";

class Jokes extends Component {
    constructor(props){
        super(props)
    }

    state = {
        jokes: [],
    };

    getJoke = () => {
        fetch('https://api.chucknorris.io/jokes/random?_limit=5').then(response => {
            if(response.ok) {
                return response.json();
            }

            throw new Error('Something wrong with network request');
        }).then(joke => {
            const newPosts = [...this.state.jokes, joke];
            this.setState({jokes: newPosts})

        }).then(error => {
            console.log(error);
        })
    };
    render() {
        return (
            <div>

                <button type="button" onClick={this.getJoke}>submit</button>
                <Item/>
                {this.state.jokes.map((joke,id) => (
                    <Item
                    key={id}
                    value={joke.value}
                    />
                ))}
            </div>
        );
    }
}

export default Jokes;