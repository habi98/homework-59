import React, { Component } from 'react';

import './App.css';
import AddMovieForm from "./components/AddMovieForm/AddMovieForm";
import Movie from "./components/Movie/Movie";

class App extends Component {
  state = {
      currentCount: 0,
      value: '',
      movie: [],
  };

  addMovieHandler = () => {
     let newMovie = {name: this.state.value, count: this.state.currentCount + 1};
     let movie = [...this.state.movie, newMovie];

     this.setState({
         movie: movie, value: '',
         currentCount: this.state.currentCount +1
     })
  };

  removedMovieHandler = (count) => {
      let movie = [...this.state.movie];
      let index = movie.findIndex((item) => item.count === count);
      movie.splice(index, 1);

      this.setState({
           movie:  movie,
          currentCount: this.state.currentCount - 1,
      })
  };

  changeMovie = (name, id) => {
      const movies = this.state.movie;
      const movie = movies[id].name = name;
      this.setState({movie: movies})
  };

    inputChangeHandler = (value) => (
        this.setState({value})
    );
  render() {
    return (
        <div className="containers-app">
            <AddMovieForm
            value={this.state.value}
            changed={this.inputChangeHandler}
            AddMovie={this.addMovieHandler}
            >
                <p>To watch list:</p>
            </AddMovieForm>

            {this.state.movie.map((movie, id) => <Movie  name={movie.name} onChange={(e) => this.changeMovie(e.target.value, id)} count={movie.count}  key={id} removed={this.removedMovieHandler}/>)}
        </div>

    );
  }
}

export default App;
