import React, {Component} from 'react';
import './Movie.css'


export default class Movie extends Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.name !== this.props.name
    }

    render() {
        return (
       <div className="block-movie clearfix">
           <div className='movie'>
              <p><input type="text" value={this.props.name} onChange={this.props.onChange} className='input-change'/></p>

           </div>
           <div className="div-button">
               <button className="button-removed" onClick={() => this.props.removed(this.props.count)}>x</button>
           </div>
       </div>


        )
    }
}
