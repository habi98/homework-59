import React from 'react';
import './AddMovieForm.css'

const AddMovieForm = (props) => {
    return (
        <div className="block-form">
            <input type="text" className="movie-form" value={props.value} onChange={(e) => props.changed(e.target.value)}/>
            <button type="button" className="add-movie" onClick={props.AddMovie}>Add</button>
            {props.children}
        </div>
    );
};

export default AddMovieForm;